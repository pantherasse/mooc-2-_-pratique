## Penser - Concevoir - Élaborer
### Niveau 2 -- Créer

- Construire une activité élève de compréhension en utilisant des exercices appropriés au concept (en s'inspirant d'une activité existante ou à partir d'une idée)
- Construire une activité élève de production (en s'inspirant d'une activité existante ou à partir d'une idée)
- Construire une séquence
