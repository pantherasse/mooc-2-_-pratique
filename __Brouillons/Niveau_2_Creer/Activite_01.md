# Traitement des données en table

Dans la section précédente, vous avez, lors de l'[activité 1](../Niveau_1_Utiliser/Activite_01.md), analysé et critiqué une première activité sur les fichiers texte et les données en tables. Proposez une suite qui permette de manipuler plus avant les données en table en utilisant les données sur les prénoms introduites à la fin de l'activité 1 (on pourra utiliser les tables `dpt2019.csv` et/ou `nat2019.csv` du dossier compressé que vous avez téléchargé précédemment).

Comme précédemment, précisez :

- la fiche élève
- la fiche prof
