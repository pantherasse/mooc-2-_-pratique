# Fichiers textes, format CSV

**But de l'activité :** manipuler avec le langage Python des fichiers textes, notamment au format CSV. Ouvrir ce genre de fichier en lecture, en récupérer le contenu dans des structures adaptées (listes, dictionnaires) ; créer aussi ce type de fichiers.

## Du tableur au fichier texte

Nous allons ouvrir un fichier texte en lecture, récupérer des données, les transformer en données utilisables, créer d'autres données puis ouvrir en _ajout_ le fichier pour venir y écrire une nouvelle ligne.

### QUESTION 1

A l'aide d'un tableur, ouvrez le fichier `binome.ods`. Vous devriez voir :

![fichier ods](Images/activite_01_ods.png)

A part la première, chaque ligne est construite comme ceci :

- la première et la dernière valeur valent 1
- chaque valeur intermédiaire est la somme des valeurs qui se trouvent juste au-dessus et au-dessus à gauche

Dans la suite de cette partie, nous allons voir comment générer de nouvelles lignes automatiquement en utilisant un programme Python.

### QUESTION 2

Un programme Python manipule des fichiers texte. Ouvrez `binome.ods` avec votre éditeur de texte et constatez qu'il est illisible. Ouvrez `binome.csv` avec votre éditeur ; vous devriez avoir quelque chose de similaire à :

![fichier csv](Images/activite_01_csv.png)

_Remarque_ : depuis le tableur on peut enregistrer un fichier au format csv.

**A faire** 

Écrire les instructions pour ouvrir le fichier `binome.csv` en lecture et sauver dans une variable `l_lignes` la liste de toutes les lignes du fichier.

_Rappel_ : la méthode pour récupérer toutes les lignes est `readlines()`

Vous pouvez utiliser le code ci-dessous pour vérifier que tout est en ordre :

```python
assert l_lignes[0] == '1,,,,\n', 'souci sur la ligne 0'
assert l_lignes[-1] == '1,4,6,4,1\n', 'souci sur la dernière ligne'
print('Tout est ok')
```

Faites en une fonction `lecture` dont le paramètre est le nom du fichier.

### QUESTION 3

On se donne la fonction `avoir_une_liste` qui prend une chaîne de caractères comme celles des lignes du fichier et qui la transforme en une liste d'entiers :

```python
def avoir_une_liste(s):
    return [int(e) for e in s.split(',') if e != '' and e != '\n']
```

Que vaut `avoir_une_liste('1,3,3,1,\n')` ? 

**A faire**

Compléter la fonction `nouvelle_liste` qui prend une liste comme celle vue précédemment et qui calcule la nouvelle liste. Par exemple pour `[1, 3, 3, 1]` la nouvelle liste est `[1, 4, 6, 4, 1]` (on remarque qu'une valeur autre que 1 est la somme de la valeur juste au-dessus de la ligne précédente avec celle au-dessus et à gauche) :

```python
def nouvelle_ligne(l):
    nouvelle = [1] * (len(l) + 1)
    for i in range(1, len(l)):
        nouvelle[i] = ... # ici remplacer les ... par ce qu'il faut
    ... # ici aussi
```

Vous pouvez vous servir du code ci-dessous pour tester votre fonction :

```python
assert nouvelle_ligne([1]) == [1, 1], 'Test 1, échec'
assert nouvelle_ligne([1, 3, 3, 1]) == [1, 4, 6, 4, 1], 'Test 2, échec'
assert nouvelle_ligne([1, 5, 10, 10, 5, 1]) == [1, 6, 15, 20, 15, 6, 1], 'Test 3, échoué'
print('Tout est ok')
```

### QUESTION 4

Nous allons maintenant transformer une liste en une chaîne de caractères grâce à la fonction `join` :

```python
def avoir_une_str(liste):
    return ','.join(str(e) for e in liste)
```

**A faire**

Ecrire une fonction `ajoute` qui écrit à la fin d'un fichier csv dont le nom est passé en paramètre une chaîne de caractères passée en deuxième paramètre. 

_Rappel_ : Il faut utiliser le mode d'ouverture `'a'` pour ajouter une ligne à un fichier. Ne pas oublier le retour à la ligne `'\n'` à la fin de la chaîne que vous allez ajouter.

Voici la fonction qui utilise ce que nous avons vu pour ajouter une ligne au fichier `binome.csv` :

```python
def ajout_binome(filename):
    l_lignes = lecture(filename)
    l_liste = avoir_une_liste(l_lignes[-1])
    nouvelle = nouvelle_ligne(l_liste)
    ajoute(filename, avoir_une_str(nouvelle))
```

Exécutez cette fonction sur notre fichier `binome.csv` et ouvrez le dans un éditeur de texte pour constater qu'il contient une ligne de plus.

### QUESTION 5

Tenter d'ouvrir le fichier `dpt2019.csv` avec un tableur. Quel avertissement nous donne l'application ? Utiliser la fonction `lecture` écrite à la question 2 pour charger les lignes du  fichier dans une liste Python que l'on nommera `prenoms`.

Combien de lignes comporte la liste `prenoms` ?

### QUESTION 6

Afficher la première ligne de `prenoms`. 

A quoi correspond cette ligne ? Expliquer le lien entre cette lignes et les autres.

Afficher la deuxième ligne de `prenoms`. Interpréter.

### QUESTION 7

Parcourir la liste `prenoms` et n'afficher que les éléments qui contiennent votre propre prénom (en majuscules). Y'en a-t-il beaucoup ? 