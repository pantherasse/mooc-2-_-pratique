# Observer, Analyser, Évaluer

> L’évaluation des élèves est au principe de la fabrication des classements, dont dépendent la réussite ou l’échec scolaires, qui commandent à leur tour l’orientation, la sélection ou la certification.
>
> À cette logique traditionnelle s’oppose, depuis les années 1970, une logique de la régulation des apprentissages, l’évaluation devenant formative, dans le cadre d’une pédagogie différenciée.
>
> Cette seconde logique reste encore marginale. Y a-t-il des raisons de penser qu’elle prendra un jour la place principale, que l’évaluation certificative en deviendra un simple corollaire, que les décisions d’orientation prolongeront les stratégies de différenciation ?
>
> Pour aller dans ce sens, il faut passer d’une évaluation orientée vers la mesure à une évaluation orientée vers la régulation et la communication, mais aussi lier intimement didactique, différenciation de l’enseignement et observation formative. Cela ne va pas sans mettre en question le contrat pédagogique, la gestion de classe, les modes de communication entre enseignants et apprenants, la nature du curriculum et de l’excellence scolaire.

Philippe Perrenoud, _L'évaluation des élèves : de la fabrication de l'excellence à la régulation des apprentissages. Entre deux logiques_

## Exemple de ce qui pourrait être demandé

- Proposer un ensemble d'évaluations (courte en classe, devoir maison etc.) sur une ou plusieurs activités vues précédemment
- Proposer une évaluation conséquente type _Bac blanc_ sur une partie conséquente du programme   
- Proposer une grille d'évaluation pour un projet
- Proposer une présentation et des indicateurs pour l'institution et la famille 
