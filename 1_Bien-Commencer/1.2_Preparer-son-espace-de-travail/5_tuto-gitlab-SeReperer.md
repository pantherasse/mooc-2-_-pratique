# Se repérer dans GitLab #

Au départ, quand on prend ses marques, ce n'est pas si simple de se retrouver dans cet espace de fichiers.
Nous vous proposons deux leviers :
   - bien noter en haut de la page que la localisation apparaît : sur quelle branche, dans quel répertoire et sous-répertoire,
   - une astuce est aussi d'ajouter des petits fichiers README.md pour dire en quelques mots (en syntaxe [markdown](https://fr.wikipedia.org/wiki/Markdown)) ce qu'on va trouver à un endroit de cette arborescence
